DROP DATABASE IF EXISTS GEE;

CREATE DATABASE GEE;

USE GEE;

DROP TABLE USUARIOS;

CREATE TABLE USUARIOS (
  id INT NOT NULL AUTO_INCREMENT,
  rol CHAR(1) NOT NULL,
  alias VARCHAR(25) NOT NULL,
  clave VARCHAR(250) NOT NULL,
  verificado BIT(1) NOT NULL,
  fecha_creacion DATETIME NOT NULL,
  fecha_actualizacion DATETIME DEFAULT NULL,
  estado BIT(1) NOT NULL,
  fecha_ultima_sesion DATETIME DEFAULT NULL,
  id_usuario_creacion INT DEFAULT NULL,
  correo VARCHAR(100) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX IDXU1 (alias),
  UNIQUE INDEX IDXU2 (correo)
);

INSERT INTO USUARIOS (rol, alias, clave, verificado, fecha_creacion, fecha_actualizacion, estado, correo)
VALUES ('A', 'admin', 'Dri+yDiXwMiumr/HthrqLw==', 1, NOW(), NOW(), 1, 'stzdiego@gmail.com' );

SELECT * FROM USUARIOS;

CREATE TABLE EMPRESAS (
  id INT NOT NULL AUTO_INCREMENT,
  nit INT NOT NULL,
  razon_social VARCHAR(100) NOT NULL,
  fecha_fundacion DATE NOT NULL,
  alias VARCHAR(25) NOT NULL,
  regimen CHAR(1) NOT NULL,
  categoria CHAR(1) NOT NULL,
  numero_contacto VARCHAR(100) NOT NULL,
  numero_contacto_aux VARCHAR(100) DEFAULT NULL,
  correo_contacto VARCHAR(50) NOT NULL,
  nombre_contacto VARCHAR(50) NOT NULL,
  cargo_contacto VARCHAR(50) NOT NULL,
  sitio_web VARCHAR(100) DEFAULT NULL,
  mision VARCHAR(500) NOT NULL,
  vision VARCHAR(500) NOT NULL,
  valores VARCHAR(250) NOT NULL,
  prom_salarial INT NOT NULL,
  pais VARCHAR(50) NOT NULL,
  ciudad VARCHAR(50) NOT NULL,
  tipo_empresa VARCHAR(20) NOT NULL,
  num_empleados INT NOT NULL,
  direccion VARCHAR(50) NOT NULL,
  facebook VARCHAR(50) DEFAULT NULL,
  twitter VARCHAR(50) DEFAULT NULL,
  instagram VARCHAR(50) DEFAULT NULL,
  linked_in VARCHAR(50) DEFAULT NULL,
  id_usuario INT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_usuario) REFERENCES USUARIOS(id)
);


CREATE TABLE CANDIDATOS (
  id INT NOT NULL AUTO_INCREMENT,
  cedula INT NOT NULL,
  nombre VARCHAR(20) NOT NULL,
  apellido VARCHAR(20) NOT NULL,
  genero CHAR(1) NOT NULL,
  descripcion VARCHAR(500),
  fecha_nacimiento DATE NOT NULL,
  estatura INT NOT NULL,
  peso INT NOT NULL,
  estado_civil CHAR(1) NOT NULL,
  foto LONGBLOB, --
  telefono VARCHAR(100) NOT NULL,
  direccion VARCHAR(50) NOT NULL,
  facebook VARCHAR(50) DEFAULT NULL,
  twitter VARCHAR(50) DEFAULT NULL,
  instagram VARCHAR(50) DEFAULT NULL,
  linked_in VARCHAR(50) DEFAULT NULL,
  ind_vehiculo BIT(1) NOT NULL,
  idioma_nativo VARCHAR(150) NOT NULL,
  pais_nacimiento VARCHAR(50) NOT NULL,
  departamento_nacimiento VARCHAR(50) NOT NULL,
  ciudad_nacimiento VARCHAR(50) NOT NULL,
  pais_residencia VARCHAR(50) NOT NULL,
  departamento_residencia VARCHAR(50) NOT NULL,
  ciudad_residencia VARCHAR(50) NOT NULL,
  id_usuario INT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_usuario) REFERENCES USUARIOS(id)
);

CREATE TABLE COMPETENCIAS (
  id INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(50) NOT NULL,
  categoria VARCHAR(25) NOT NULL,
  certificada BIT(1) NOT NULL,
  criterio_medicion VARCHAR(250) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE OFERTAS (
  id INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(30) NOT NULL,
  salario_base DECIMAL(10, 2) NOT NULL,
  salario_maximo DECIMAL(10, 2) DEFAULT NULL,
  descripcion VARCHAR(500) NOT NULL,
  dias_semanales INT NOT NULL,
  modalidad CHAR(1) NOT NULL,
  expiracion DATE NOT NULL,
  vacantes INT NOT NULL,
  cargo VARCHAR(255) NOT NULL,
  estado CHAR(1) NOT NULL,
  tipo_contrato VARCHAR(20) NOT NULL,
  adicionales VARCHAR(250) DEFAULT NULL,
  ind_experiencia BIT(1) NOT NULL,
  experiencia_minima INT DEFAULT NULL,
  id_empresa INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_empresa) REFERENCES EMPRESAS(id)
);


CREATE TABLE ESTUDIOS (
  id INT NOT NULL AUTO_INCREMENT,
  nombre VARCHAR(30) NOT NULL,
  institucion VARCHAR(30) NOT NULL,
  tiempo INT NOT NULL,
  ubicacion VARCHAR(20) NOT NULL,
  fecha_inicio DATE NOT NULL,
  fecha_fin DATE NULL,
  financiamiento DECIMAL(10,2)NOT NULL,
  modalidad VARCHAR(20)NOT NULL,
  descripcion VARCHAR(30)NOT NULL,
  profesional BIT(1)NOT NULL,
  certificado LONGBLOB NOT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE EXPERIENCIA (
  id INT NOT NULL AUTO_INCREMENT,
  tipo CHAR(1) NOT NULL,
  historial_empresas VARCHAR(50) DEFAULT NULL,
  cargo VARCHAR(50) NOT NULL,
  descripcion VARCHAR(255) NOT NULL,
  tiempo INT NOT NULL,
  nombre_jefe VARCHAR(50) DEFAULT NULL,
  telefono_contacto VARCHAR(100) NOT NULL,
  correo_contacto VARCHAR(100) NOT NULL,
  logros VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE POSTULACIONES (
  id_candidato INT NOT NULL,
  id_oferta INT NOT NULL,
  fecha_registro DATE NOT NULL,
  comentario_candidato VARCHAR(255),
  comentario_empresa VARCHAR(255),
  PRIMARY KEY (id_candidato, id_oferta),
  FOREIGN KEY (id_candidato) REFERENCES CANDIDATOS(id),
  FOREIGN KEY (id_oferta) REFERENCES OFERTAS(id)
);

CREATE TABLE HABILIDADES (
  id_competencia INT NOT NULL,
  id_candidato INT NOT NULL,
  nivel INT,
  certificado BLOB,
  comentario VARCHAR(255),
  PRIMARY KEY (id_competencia, id_candidato),
  FOREIGN KEY (id_competencia) REFERENCES COMPETENCIAS(id),
  FOREIGN KEY (id_candidato) REFERENCES CANDIDATOS(id)
);

CREATE TABLE ENTREVISTAS (
  id INT NOT NULL AUTO_INCREMENT,
  id_candidato INT NOT NULL,
  id_oferta INT NOT NULL,
  consecutivo INT,
  comentario_empresa VARCHAR(255),
  calificacion INT,
  ind_contratado BIT(1),
  PRIMARY KEY (id),
  FOREIGN KEY (id_candidato, id_oferta, ) REFERENCES POSTULACIONES(id_candidato, id_oferta),
  CHECK (calificacion >= 0 AND calificacion <= 10)
);

CREATE TABLE REQUERIMIENTOS (
	id_oferta INT NOT NULL,
	id_competencia INT NOT NULL,
	nivel_minimo INT NOT NULL,
	comentario VARCHAR(255) NULL,
	PRIMARY KEY (id_oferta, id_competencia),
	FOREIGN KEY (id_oferta) REFERENCES OFERTAS(id),
	FOREIGN KEY (id_competencia) REFERENCES COMPETENCIAS(id)
);



