/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Class;

import Entities.Enums;
import Entities.Enums.Rol;

public class Convertions {
    
    public static String RolToString(Rol rol)
    {
        switch(rol)
        {
            case Administrator :
                return "A";
            case Candidate:
                return "C";
            case Company:
                return "E";
            default: 
                throw new AssertionError();
        }
    }
    
    public static Rol StringToRol(String str)
    {
        switch (str) {
            case "A":
                return Enums.Rol.Administrator;
            case "C":
                return Enums.Rol.Candidate;
            case "E":
                return Enums.Rol.Company;
            default:
                throw new AssertionError();
        }
    }
    
    public static String StringToRolString(String str)
    {
        switch (str) {
            case "Administrator":
                return "A";
            case "Candidato":
                return "C";
            case "Empresa":
                return "E";
            default:
                throw new AssertionError();
        }
    }
    
}
