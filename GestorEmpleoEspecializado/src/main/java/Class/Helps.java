/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Class;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.toedter.calendar.JDateChooser;
import java.util.regex.Pattern;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author diegosantacruz
 */
public class Helps {
    
    private static final String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);
    
    public static boolean IsNullOrEmpty(String str)
    {
        return str.isEmpty();
    }
    
    public static boolean ValidateEmail(String str)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                        "[a-zA-Z0-9_+&*-]+)*@" +
                        "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                        "A-Z]{2,7}$";
    Pattern pattern = Pattern.compile(emailRegex);
    return !pattern.matcher(str).matches();
    }
    
    public static boolean ValidateNumber(String str)
    {
        String numberRegex = "\\d+";
    Pattern pattern = Pattern.compile(numberRegex);
    return !pattern.matcher(str).matches();
    }
    
    public static boolean ValidateText(String str)
    {
        String numberRegex = "^[a-zA-Z]+$";
    Pattern pattern = Pattern.compile(numberRegex);
    return !pattern.matcher(str).matches();
    }
    
    public static boolean ValidateYears(JDateChooser dateChooser, int years) {
        // Obtener la fecha seleccionada del JDateChooser
        Date fechaSeleccionada = dateChooser.getDate();
        
        // Obtener la fecha actual
        Calendar calendar = Calendar.getInstance();
        Date fechaActual = calendar.getTime();
        
        // Calcular la diferencia de años entre la fecha actual y la fecha seleccionada
        calendar.setTime(fechaSeleccionada);
        int anioSeleccionado = calendar.get(Calendar.YEAR);
        int mesSeleccionado = calendar.get(Calendar.MONTH);
        int diaSeleccionado = calendar.get(Calendar.DAY_OF_MONTH);
        
        calendar.setTime(fechaActual);
        int anioActual = calendar.get(Calendar.YEAR);
        int mesActual = calendar.get(Calendar.MONTH);
        int diaActual = calendar.get(Calendar.DAY_OF_MONTH);
        
        int diferenciaAnios = anioActual - anioSeleccionado;
        
        // Verificar si la diferencia de años es mayor o igual a 18
        if (diferenciaAnios > years) {
            return true;
        } else if (diferenciaAnios == years) {
            // Si tienen la misma edad, comprobar el mes y día para asegurarse de que ya cumplió los 18
            if (mesActual > mesSeleccionado) {
                return true;
            } else if (mesActual == mesSeleccionado && diaActual >= diaSeleccionado) {
                return true;
            }
        }
        
        return false;
    }
    
    //------------------------------------
    private static final String API_URL_TOKEN = "https://www.universal-tutorial.com/api/getaccesstoken";
    private static final String API_URL_COUNTRY = "https://www.universal-tutorial.com/api/countries/";
    private static final String API_URL_STATE = "https://www.universal-tutorial.com/api/states/";
    private static final String API_URL_CITY = "https://www.universal-tutorial.com/api/cities/";

    public static List<String> getCountries() throws IOException, Exception {
        StringBuilder response;
        String line;
        String Token;
        BufferedReader reader;
        HttpURLConnection connection;
        int responseCode;
        Gson gson;
        List<String> countries;

        gson = new Gson();
        connection = (HttpURLConnection) new URL(API_URL_TOKEN).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("api-token", "jAiZGyrUnFsApkTUZeDWGyBCzwGpTwXaEm7NN-FUDhCzHMVMAc3q_ImW8m5GirvxaD4");
        connection.setRequestProperty("user-email", "gesempesp@gmail.com");
        
        responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            response = new StringBuilder();
            while ((line = reader.readLine()) != null) response.append(line);
            reader.close();
            Token = gson.fromJson(response.toString(), JsonObject.class).get("auth_token").getAsString();
            
            connection = (HttpURLConnection) new URL(API_URL_COUNTRY).openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", "Bearer " + Token);
            connection.setRequestProperty("Accept", "application/json");
            
            responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuilder();
                while ((line = reader.readLine()) != null) response.append(line);
                reader.close();
                countries = new ArrayList();
                        
                for(JsonElement json : gson.fromJson(response.toString(), JsonArray.class))
                {
                    countries.add(json.getAsJsonObject().get("country_name").getAsString());
                }
                
                return countries;
            }
            
            throw new Exception("No se logro conectar con la API donde se obtienen los paises, consultelo con su administrador.");
        }
        else
        {
            throw new Exception("No se logro conectar con la API donde se obtienen los paises, consultelo con su administrador.");
        }
    }
    
    public static List<String> getStates(String country) throws IOException, Exception {
        StringBuilder response;
        String line;
        String Token;
        BufferedReader reader;
        HttpURLConnection connection;
        int responseCode;
        Gson gson;
        List<String> states;

        gson = new Gson();
        connection = (HttpURLConnection) new URL(API_URL_TOKEN).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("api-token", "jAiZGyrUnFsApkTUZeDWGyBCzwGpTwXaEm7NN-FUDhCzHMVMAc3q_ImW8m5GirvxaD4");
        connection.setRequestProperty("user-email", "gesempesp@gmail.com");
        
        responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            response = new StringBuilder();
            while ((line = reader.readLine()) != null) response.append(line);
            reader.close();
            Token = gson.fromJson(response.toString(), JsonObject.class).get("auth_token").getAsString();
            
            connection = (HttpURLConnection) new URL(API_URL_STATE + country).openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", "Bearer " + Token);
            connection.setRequestProperty("Accept", "application/json");
            
            responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuilder();
                while ((line = reader.readLine()) != null) response.append(line);
                reader.close();
                states = new ArrayList();
                        
                for(JsonElement json : gson.fromJson(response.toString(), JsonArray.class))
                {
                    states.add(json.getAsJsonObject().get("state_name").getAsString());
                }
                
                return states;
            }
            
            throw new Exception("No se logro conectar con la API donde se obtienen los paises, consultelo con su administrador.");
        }
        else
        {
            throw new Exception("No se logro conectar con la API donde se obtienen los paises, consultelo con su administrador.");
        }
    }
    
    public static List<String> getCities(String state) throws IOException, Exception {
        StringBuilder response;
        String line;
        String Token;
        BufferedReader reader;
        HttpURLConnection connection;
        int responseCode;
        Gson gson;
        List<String> states;

        gson = new Gson();
        connection = (HttpURLConnection) new URL(API_URL_TOKEN).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("api-token", "jAiZGyrUnFsApkTUZeDWGyBCzwGpTwXaEm7NN-FUDhCzHMVMAc3q_ImW8m5GirvxaD4");
        connection.setRequestProperty("user-email", "gesempesp@gmail.com");
        
        responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            response = new StringBuilder();
            while ((line = reader.readLine()) != null) response.append(line);
            reader.close();
            Token = gson.fromJson(response.toString(), JsonObject.class).get("auth_token").getAsString();
            
            connection = (HttpURLConnection) new URL(API_URL_CITY + state).openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", "Bearer " + Token);
            connection.setRequestProperty("Accept", "application/json");
            
            responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuilder();
                while ((line = reader.readLine()) != null) response.append(line);
                reader.close();
                states = new ArrayList();
                        
                for(JsonElement json : gson.fromJson(response.toString(), JsonArray.class))
                {
                    states.add(json.getAsJsonObject().get("city_name").getAsString());
                }
                
                return states;
            }
            
            throw new Exception("No se logro conectar con la API donde se obtienen los paises, consultelo con su administrador.");
        }
        else
        {
            throw new Exception("No se logro conectar con la API donde se obtienen los paises, consultelo con su administrador.");
        }
    }
    //------------------------------------
    
}

