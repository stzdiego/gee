/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

/**
 *
 * @author diegosantacruz
 */
public class Habilities {
    public int idCompetencia;
    public int idCandidato;
    public int nivel;
    public byte[] certificado;
    public String comentario;
}
