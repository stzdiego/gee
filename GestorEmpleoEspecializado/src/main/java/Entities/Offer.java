/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author diegosantacruz
 */
public class Offer {
    public int Id;
    public String Nombre;
    public BigDecimal SalarioBase;
    public BigDecimal SalarioMaximo;
    public String Descripcion;
    public int DiasSemanales;
    public char Modalidad;
    public Date Expiracion;
    public int Vacantes;
    public String Cargo;
    public char Estado;
    public String TipoContrato;
    public String Adicionales;
    public boolean IndExperiencia;
    public Integer ExperienciaMinima;
    public int IdEmpresa;
    public String NombreEmpresa;
    public int NumRequerimientos;

    public Offer() {
    }

    public Offer(int Id, String Nombre, BigDecimal SalarioBase, BigDecimal SalarioMaximo, String Descripcion, int DiasSemanales, char Modalidad, Date Expiracion, int Vacantes, String Cargo, char Estado, String TipoContrato, String Adicionales, boolean IndExperiencia, Integer ExperienciaMinima, int IdEmpresa) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.SalarioBase = SalarioBase;
        this.SalarioMaximo = SalarioMaximo;
        this.Descripcion = Descripcion;
        this.DiasSemanales = DiasSemanales;
        this.Modalidad = Modalidad;
        this.Expiracion = Expiracion;
        this.Vacantes = Vacantes;
        this.Cargo = Cargo;
        this.Estado = Estado;
        this.TipoContrato = TipoContrato;
        this.Adicionales = Adicionales;
        this.IndExperiencia = IndExperiencia;
        this.ExperienciaMinima = ExperienciaMinima;
        this.IdEmpresa = IdEmpresa;
    }
    
    public static DefaultTableModel GetTableModel(List<Offer> offers) {
        DefaultTableModel model;
        String[] columns;

        columns = new String[]{"ID", "Empresa", "Descripción", "Salario base", "Vacantes", "Dias semanales", "Vencimiento", "Requerimientos"};
        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Hacer que todas las celdas sean no editables
            }
        };

        for (Offer offer : offers) {
            Object[] fila = {offer.Id, offer.NombreEmpresa, offer.Descripcion, offer.SalarioBase, offer.Vacantes, offer.DiasSemanales, offer.Expiracion, offer.NumRequerimientos};
            model.addRow(fila);
        }

        return model;
    }
}
