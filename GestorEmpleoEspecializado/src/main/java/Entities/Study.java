/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

import java.sql.Date;

/**
 *
 * @author diegosantacruz
 */
public class Study {
    public int Id;
    public String Nombre;
    public String Institucion;
    public int Tiempo;
    public String Ubicacion;
    public Date FechaInicio;
    public Date FechaFin;
    public String Modalidad;
    public String Descripcion;
    public boolean Profesional;
    public byte[] Certificado;
}
