/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

import java.sql.Date;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author diegosantacruz
 */
public class User {
    
    public int Id;
    public Enums.Rol Rol;
    public String Alias;
    public String Correo;
    public String Clave;
    public boolean Verificado;
    public Date FechaCreacion;
    public Date FechaActualizacion;
    public boolean Estado;
    public Date FechaUltimaSesion;
    public int IdUsuarioCreacion;
    
    public User()
    {
        
    }

    public User(int Id, Enums.Rol Rol, String Alias, String Correo, String Clave, boolean Verificado, Date FechaCreacion, Date FechaActualizacion, boolean Estado, Date FechaUltimaSesion, int IdUsuarioCreacion) {
        this.Id = Id;
        this.Rol = Rol;
        this.Alias = Alias;
        this.Correo = Correo;
        this.Clave = Clave;
        this.Verificado = Verificado;
        this.FechaCreacion = FechaCreacion;
        this.FechaActualizacion = FechaActualizacion;
        this.Estado = Estado;
        this.FechaUltimaSesion = FechaUltimaSesion;
        this.IdUsuarioCreacion = IdUsuarioCreacion;
    }
    
    public static DefaultTableModel GetTableModel(List<User> users)
    {
        DefaultTableModel model;
        String[] columns;
        
        columns = new String[] {"ID", "Rol", "Alias", "Correo", "Verificado", "Estado", "Creación", "Actualización", "Ultima sesión", "Creador"};
        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Hacer que todas las celdas sean no editables
            }
        };
        
        for (User user : users) {
            Object[] fila = { user.Id, user.Rol, user.Alias, user.Correo, user.Verificado, user.Estado, user.FechaCreacion, user.FechaActualizacion, user.FechaUltimaSesion, user.IdUsuarioCreacion };
            model.addRow(fila);
        }
        
        return model;
    }
    
}
