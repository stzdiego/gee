/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

/**
 *
 * @author diegosantacruz
 */
public class Enums {
    
    public enum Rol
    {
        Administrator,
        Candidate,
        Company
    }
    
    public enum PanelMode
    {
        Create,
        Edit
    }
    
}
