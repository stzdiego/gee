/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author diegosantacruz
 */
public class Competence {
    public int Id;
    public String Nombre;
    public String Categoria;
    public boolean Certificada;
    public String CriterioMedicion;
    
    public Competence() {
        // Constructor vacío
    }
    
    
    
    public Competence(int Id, String Nombre, String Categoria, boolean Certificada, String CriterioMedicion) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Categoria = Categoria;
        this.Certificada = Certificada;
        this.CriterioMedicion = CriterioMedicion;
    }
    
    // Método GetTableModel para generar un DefaultTableModel a partir de una lista de Competence
    
    public static DefaultTableModel GetTableModel(List<Competence> competences) {
        DefaultTableModel model;
        String[] columns = {"ID", "Nombre", "Categoría", "Certificada", "Criterio de Medición"};
        
        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Hacer que todas las celdas sean no editables
            }
        };
        
        for (Competence competence : competences) {
            Object[] rowData = {competence.Id, competence.Nombre, competence.Categoria, competence.Certificada, competence.CriterioMedicion};
            model.addRow(rowData);
        }
        
        return model;
    }
}
