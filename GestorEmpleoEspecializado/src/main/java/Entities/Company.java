/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author diegosantacruz
 */
public class Company {
    public int Id;
    public int Nit;
    public String RazonSocial;
    public Date FechaFundacion;
    public String Alias;
    public char Regimen;
    public char Categoria;
    public int NumeroContacto;
    public int NumeroContactoAux;
    public String CorreoContacto;
    public String NombreContacto;
    public String CargoContacto;
    public String SitioWeb;
    public String Mision;
    public String Vision;
    public String Valores;
    public int PromSalarial;
    public String Pais;
    public String Ciudad;
    public String TipoEmpresa;
    public int NumEmpleados;
    public String Direccion;
    public String Facebook;
    public String Twitter;
    public String Instagram;
    public String LinkedIn;
    public int IdUsuario;
    public String Departamento;

    public Company() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public int GetAge() {
        LocalDate fechaActual = LocalDate.now();
        Period periodo = Period.between(FechaFundacion.toLocalDate(), fechaActual);
        return periodo.getYears();
    }

    public Company(int Id, int Nit, String RazonSocial, Date FechaFundacion, String Alias, char Regimen, char Categoria, int NumeroContacto, int NumeroContactoAux, String CorreoContacto, String NombreContacto, String CargoContacto, String SitioWeb, String Mision, String Vision, String Valores, int PromSalarial, String Pais, String Ciudad, String TipoEmpresa, int NumEmpleados, String Direccion, String Facebook, String Twitter, String Instagram, String LinkedIn, int IdUsuario, String Departamento) {
        this.Id = Id;
        this.Nit = Nit;
        this.RazonSocial = RazonSocial;
        this.FechaFundacion = FechaFundacion;
        this.Alias = Alias;
        this.Regimen = Regimen;
        this.Categoria = Categoria;
        this.NumeroContacto = NumeroContacto;
        this.NumeroContactoAux = NumeroContactoAux;
        this.CorreoContacto = CorreoContacto;
        this.NombreContacto = NombreContacto;
        this.CargoContacto = CargoContacto;
        this.SitioWeb = SitioWeb;
        this.Mision = Mision;
        this.Vision = Vision;
        this.Valores = Valores;
        this.PromSalarial = PromSalarial;
        this.Pais = Pais;
        this.Ciudad = Ciudad;
        this.TipoEmpresa = TipoEmpresa;
        this.NumEmpleados = NumEmpleados;
        this.Direccion = Direccion;
        this.Facebook = Facebook;
        this.Twitter = Twitter;
        this.Instagram = Instagram;
        this.LinkedIn = LinkedIn;
        this.IdUsuario = IdUsuario;
        this.Departamento = Departamento;
    }
    
    

    public static DefaultTableModel GetTableModel(List<Company> companies) {
        DefaultTableModel model;
        String[] columns;

        columns = new String[]{"ID", "NIT", "Razón Social", "Fecha de Fundación", "Alias", "Regimen", "Categoria", "Número de Contacto", "Número de Contacto Auxiliar", "Correo de Contacto", "Nombre de Contacto", "Cargo de Contacto", "Sitio Web", "Misión", "Visión", "Valores", "Promedio Salarial", "País", "Departamento", "Ciudad", "Tipo de Empresa", "Número de Empleados", "Dirección", "Facebook", "Twitter", "Instagram", "LinkedIn", "ID de Usuario"};
        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Hacer que todas las celdas sean no editables
            }
        };

        for (Company company : companies) {
            Object[] fila = {
                company.Id,
                company.Nit,
                company.RazonSocial,
                company.FechaFundacion,
                company.Alias,
                company.Regimen,
                company.Categoria,
                company.NumeroContacto,
                company.NumeroContactoAux,
                company.CorreoContacto,
                company.NombreContacto,
                company.CargoContacto,
                company.SitioWeb,
                company.Mision,
                company.Vision,
                company.Valores,
                company.PromSalarial,
                company.Pais,
                company.Departamento,
                company.Ciudad,
                company.TipoEmpresa,
                company.NumEmpleados,
                company.Direccion,
                company.Facebook,
                company.Twitter,
                company.Instagram,
                company.LinkedIn,
                company.IdUsuario
            };
            model.addRow(fila);
        }

        return model;
    }

    public static DefaultTableModel GetTableModelListCompany(List<Company> companies) {
        DefaultTableModel model;
        String[] columns;

        columns = new String[]{"ID", "Razón Social", "Alias", "Fecha de Fundación", "Número de Contacto", "Sitio Web", "País", "Ciudad", "Tipo de Empresa", "Número de Empleados"};
        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Hacer que todas las celdas sean no editables
            }
        };

        for (Company company : companies) {
            Object[] fila = {
                company.IdUsuario,
                company.RazonSocial,
                company.Alias,
                company.FechaFundacion,
                company.NumeroContacto,
                company.SitioWeb,
                company.Pais,
                company.Ciudad,
                company.TipoEmpresa,
                company.NumEmpleados
            };
            model.addRow(fila);
        }

        return model;
    }
}
