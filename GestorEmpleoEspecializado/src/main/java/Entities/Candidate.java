/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author diegosantacruz
 */
public class Candidate {
    public int Id;
    public int Cedula;
    public String Nombre;
    public String Apellido;
    public char Genero;
    public String Descripcion;
    public Date FechaNacimiento;
    public int Estatura;
    public int Peso;
    public char EstadoCivil;
    public byte[] Foto;
    public String Telefono;
    public String Direccion;
    public String Facebook;
    public String Twitter;
    public String Instagram;
    public String LinkedIn;
    public boolean IndVehiculo;
    public String IdiomaNativo;
    public String PaisNacimiento;
    public String DepartamentoNacimiento;
    public String CiudadNacimiento;
    public String PaisResidencia;
    public String DepartamentoResidencia;
    public String CiudadResidencia;
    public int IdUsuario;
    
    public Candidate() {
        // Constructor vacío
    }

    public Candidate(int Id, int Cedula, String Nombre, String Apellido, String Descripcion, Date FechaNacimiento, int Estatura, int Peso, char EstadoCivil, byte[] Foto, String Telefono, String Direccion, String Facebook, String Twitter, String Instagram, String LinkedIn, boolean IndVehiculo, String IdiomaNativo, String PaisNacimiento, String DepartamentoNacimiento, String CiudadNacimiento, String PaisResidencia, String DepartamentoResidencia, String CiudadResidencia, int IdUsuario, char Genero) {
        this.Id = Id;
        this.Cedula = Cedula;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Descripcion = Descripcion;
        this.FechaNacimiento = FechaNacimiento;
        this.Estatura = Estatura;
        this.Peso = Peso;
        this.EstadoCivil = EstadoCivil;
        this.Foto = Foto;
        this.Telefono = Telefono;
        this.Direccion = Direccion;
        this.Facebook = Facebook;
        this.Twitter = Twitter;
        this.Instagram = Instagram;
        this.LinkedIn = LinkedIn;
        this.IndVehiculo = IndVehiculo;
        this.IdiomaNativo = IdiomaNativo;
        this.PaisNacimiento = PaisNacimiento;
        this.DepartamentoNacimiento = DepartamentoNacimiento;
        this.CiudadNacimiento = CiudadNacimiento;
        this.PaisResidencia = PaisResidencia;
        this.DepartamentoResidencia = DepartamentoResidencia;
        this.CiudadResidencia = CiudadResidencia;
        this.IdUsuario = IdUsuario;
        this.Genero = Genero;
    }
    
    public int GetAge()
    {
        LocalDate fechaActual = LocalDate.now();
        Period periodo = Period.between(FechaNacimiento.toLocalDate(), fechaActual);
        return periodo.getYears();
    }
    
    public static DefaultTableModel GetTableModel(List<Candidate> candidates) {
        DefaultTableModel model;
        String[] columns;

        columns = new String[]{"ID", "Cédula", "Nombre", "Apellido", "Descripción", "Fecha de Nacimiento", "Estatura", "Peso", "Estado Civil", "Teléfono", "Dirección", "Facebook", "Twitter", "Instagram", "LinkedIn", "Vehículo", "Idioma Nativo", "País de Nacimiento", "Departamento de Nacimiento", "Ciudad de Nacimiento", "País de Residencia", "Departamento de Residencia", "Ciudad de Residencia", "ID Usuario"};
        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Hacer que todas las celdas sean no editables
            }
        };

        for (Candidate candidate : candidates) {
            Object[] fila = {candidate.Id, candidate.Cedula, candidate.Nombre, candidate.Apellido, candidate.Descripcion, candidate.FechaNacimiento, candidate.Estatura, candidate.Peso, candidate.EstadoCivil, candidate.Telefono, candidate.Direccion, candidate.Facebook, candidate.Twitter, candidate.Instagram, candidate.LinkedIn, candidate.IndVehiculo, candidate.IdiomaNativo, candidate.PaisNacimiento, candidate.DepartamentoNacimiento, candidate.CiudadNacimiento, candidate.PaisResidencia, candidate.DepartamentoResidencia, candidate.CiudadResidencia, candidate.IdUsuario};
            model.addRow(fila);
        }

        return model;
    }
    
    public static DefaultTableModel GetTableModelListCompay(List<Candidate> candidates) {
        DefaultTableModel model;
        String[] columns;

        columns = new String[]{"Id", "Nombre", "Apellido", "Edad", "Teléfono", "LinkedIn", "Idioma Nativo", "País de Residencia", "Departamento de Residencia", "Ciudad de Residencia"};
        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Hacer que todas las celdas sean no editables
            }
        };

        for (Candidate candidate : candidates) {
            Object[] fila = {candidate.IdUsuario, candidate.Nombre, candidate.Apellido, candidate.GetAge(), candidate.Telefono, candidate.LinkedIn, candidate.IdiomaNativo, candidate.PaisResidencia, candidate.DepartamentoResidencia, candidate.CiudadResidencia};
            model.addRow(fila);
        }

        return model;
    }
}
