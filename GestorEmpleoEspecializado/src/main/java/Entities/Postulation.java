/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

import java.sql.Date;

/**
 *
 * @author diegosantacruz
 */
public class Postulation {
    public int IdCandidato;
    public int IdOferta;
    public Date FechaRegistro;
    public String ComentarioCandidato;
    public String ComentarioEmpresa;
}
