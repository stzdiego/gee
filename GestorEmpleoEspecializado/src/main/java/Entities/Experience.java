/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

/**
 *
 * @author diegosantacruz
 */
public class Experience {
    public int Id;
    public char Tipo;
    public String HistorialEmpresas;
    public String Cargo;
    public String Descripcion;
    public int Tiempo;
    public String NombreJefe;
    public String TelefonoContacto;
    public String CorreoContacto;
    public String Logros;
}
