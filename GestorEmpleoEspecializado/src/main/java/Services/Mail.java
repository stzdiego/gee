/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

/**
 *
 * @author diegosantacruz
 */
public class Mail {
    
    public static void Send(String email, String subject, String htmlmessage) throws MessagingException {
        // Configuración de las propiedades de la sesión de correo electrónico
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com"); // o el servidor SMTP que uses
        props.put("mail.smtp.port", "587"); // puerto para el servidor SMTP

        // Autenticación del remitente del correo electrónico
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("tu-correo@gmail.com", "tu-contraseña");
            }
        };

        // Creación de la sesión de correo electrónico
        Session session = Session.getInstance(props, auth);

        // Creación del mensaje de correo electrónico
        Message mensajeCorreo = new MimeMessage(session);
        mensajeCorreo.setFrom(new InternetAddress("tu-correo@gmail.com")); // dirección de correo electrónico del remitente
        mensajeCorreo.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email)); // dirección de correo electrónico del destinatario
        mensajeCorreo.setSubject(subject); // asunto del correo electrónico
        
        // Creación del cuerpo del mensaje en formato HTML
        mensajeCorreo.setContent(htmlmessage, "text/html; charset=utf-8");

        // Envío del correo electrónico
        Transport.send(mensajeCorreo);
    }
    
}
