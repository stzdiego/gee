/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;

import java.awt.Component;
import javax.swing.JOptionPane;

/**
 *
 * @author diegosantacruz
 */
public class Notifications {
    
    public static void ShowInformation(Component parent, String message)
    {
        JOptionPane.showMessageDialog(parent, message, "Información", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void ShowError(Component parent, String message)
    {
        JOptionPane.showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public static void ShowWarning(Component parent, String message)
    {
        JOptionPane.showMessageDialog(parent, message, "Advertencia", JOptionPane.WARNING_MESSAGE);
    }
    
    public static boolean ShowConfirmation(Component parent, String message)
    {
        int result;
        
        result = JOptionPane.showConfirmDialog(parent, message, "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        return result == JOptionPane.YES_OPTION;
    }
    
}
