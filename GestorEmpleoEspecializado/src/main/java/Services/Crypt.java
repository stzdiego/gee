/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Services;

/**
 *
 * @author diegosantacruz
 */

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class Crypt {
    
    private static final String secretkey = "2b7e151628aed2a6abf71589";
    
    public static String Encrypt(String cadena) throws Exception {
        SecretKeySpec key = new SecretKeySpec(secretkey.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = cipher.doFinal(cadena.getBytes());
        return Base64.getEncoder().encodeToString(encVal);
    }

    public static String Decrypt(String cadenaEncriptada) throws Exception {
        SecretKeySpec key = new SecretKeySpec(secretkey.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Base64.getDecoder().decode(cadenaEncriptada);
        byte[] decValue = cipher.doFinal(decordedValue);
        return new String(decValue);
    }
    
}
