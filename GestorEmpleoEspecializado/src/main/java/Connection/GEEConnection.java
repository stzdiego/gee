/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Connection;

import Entities.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Services.Crypt;
import static Class.Convertions.*;
import static Class.Helps.IsNullOrEmpty;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author diegosantacruz
 */
public class GEEConnection {
    
    private Connection _conn;
    
    //private final String _url = "jdbc:mysql://localhost:3306/GEE";
    private final String _host = "localhost:3306";
    private final String _bd = "GEE";
    private final String _user = "root";
    private final String _password = "root";
    
    public void Connect() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        _conn = DriverManager.getConnection("jdbc:mysql://" + _host + "/" + _bd,
                _user, _password);
    }
    
    // <editor-fold defaultstate="collapsed" desc="General"> 
    public String GetConnectDescription()
    {
        return "Servidor: " + _host + " | BD: " + _bd + " | Usuario: " + _user;
    }
    
    public void Test() throws SQLException, ClassNotFoundException
    {
        Connect();
        _conn.close();
    }
    
    public boolean Login(String user, String password) throws SQLException, 
            ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        int count;
        
        query = "SELECT COUNT(*) FROM USUARIOS WHERE alias = ? AND clave = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, user);
        stmt.setString(2, Crypt.Encrypt(password));
        rs = stmt.executeQuery();
        rs.next();
        count = rs.getInt(1);
        _conn.close();
        
        return count > 0;
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="User"> 
    public List<User> GetUsers(String alias) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        List<User> users;
        
        users = new ArrayList<>();
        
        if(IsNullOrEmpty(alias))
        {
            query = "SELECT id, rol, alias, clave, verificado, fecha_creacion, "
                + "fecha_actualizacion, estado, fecha_ultima_sesion, "
                + "id_usuario_creacion, correo FROM USUARIOS";
        }
        else
        {
            query = "SELECT id, rol, alias, clave, verificado, fecha_creacion, "
                + "fecha_actualizacion, estado, fecha_ultima_sesion, "
                + "id_usuario_creacion, correo FROM USUARIOS WHERE alias LIKE ?";
        }
        
        Connect();
        stmt = _conn.prepareStatement(query);
        if(!IsNullOrEmpty(alias)) stmt.setString(1,"%" + alias + "%");
        rs = stmt.executeQuery();
        
        while (rs.next())
        {
            users.add(new User(rs.getInt("id"), StringToRol(rs.getString("rol")), rs.getString("alias"), rs.getString("correo"), rs.getString("clave"), rs.getBoolean("verificado"), rs.getDate("fecha_creacion"), rs.getDate("fecha_actualizacion"), rs.getBoolean("estado"), rs.getDate("fecha_ultima_sesion"), rs.getInt("id_usuario_creacion")));
        }
        _conn.close();
        
        return users;
    }
    
    public User GetUser(String alias) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        User user;
        
        query = "SELECT id, rol, alias, clave, verificado, fecha_creacion, "
                + "fecha_actualizacion, estado, fecha_ultima_sesion, "
                + "id_usuario_creacion, correo FROM USUARIOS WHERE alias = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, alias);
        rs = stmt.executeQuery();
        rs.next();
        
        user = new User();
        user.Id = rs.getInt("id");
        user.Rol = StringToRol(rs.getString("rol"));
        user.Alias = rs.getString("alias");
        user.Clave = rs.getString("clave");
        user.Verificado = rs.getBoolean("verificado");
        user.FechaCreacion = rs.getDate("fecha_creacion");
        user.FechaActualizacion = rs.getDate("fecha_actualizacion");
        user.Estado = rs.getBoolean("estado");
        user.FechaUltimaSesion = rs.getDate("fecha_ultima_sesion");
        user.IdUsuarioCreacion = rs.getInt("id_usuario_creacion");
        user.Correo = rs.getString("correo");
        
        _conn.close();
        
        return user;
    }
    
    public User GetUser(int id) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        User user;
        
        query = "SELECT id, rol, alias, clave, verificado, fecha_creacion, "
                + "fecha_actualizacion, estado, fecha_ultima_sesion, "
                + "id_usuario_creacion, correo FROM USUARIOS WHERE id = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, id);
        rs = stmt.executeQuery();
        rs.next();
        
        user = new User();
        user.Id = rs.getInt("id");
        user.Rol = StringToRol(rs.getString("rol"));
        user.Alias = rs.getString("alias");
        user.Clave = rs.getString("clave");
        user.Verificado = rs.getBoolean("verificado");
        user.FechaCreacion = rs.getDate("fecha_creacion");
        user.FechaActualizacion = rs.getDate("fecha_actualizacion");
        user.Estado = rs.getBoolean("estado");
        user.FechaUltimaSesion = rs.getDate("fecha_ultima_sesion");
        user.IdUsuarioCreacion = rs.getInt("id_usuario_creacion");
        user.Correo = rs.getString("correo");
        
        _conn.close();
        
        return user;
    }
    
    public User UpdateLastSession(String alias) throws SQLException, 
            ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;
        
        query = "UPDATE USUARIOS SET fecha_ultima_sesion = NOW() WHERE "
                + "alias = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, alias);
        rowupdate = stmt.executeUpdate();
        _conn.close();
        
        if (rowupdate < 0) {
                throw new Exception("No se logro actualizar la sesión "
                        + "del usuario.");
        }
        
        return GetUser(alias);
    }
    
    public void ChangePassword(User user, String password) throws SQLException, ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;
        
        query = "UPDATE USUARIOS SET fecha_actualizacion = NOW(), clave = ? WHERE id = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, Crypt.Encrypt(password));
        stmt.setInt(2, user.Id);
        rowupdate = stmt.executeUpdate();
        _conn.close();
        
        if (rowupdate < 0) {
                throw new Exception("No se logro actualizar la contraseña "
                        + "del usuario.");
        }
    }
    
    public void CreateUser(String rol, String alias, String clave, 
            boolean verificado, boolean estado, String correo, User usercreation) throws SQLException, ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;
        
        query = "INSERT INTO USUARIOS (rol, alias, clave, verificado, fecha_creacion, estado, id_usuario_creacion, correo) "
                + "VALUES (?, ?, ?, ?, NOW(), ?, ?, ?)";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        
        stmt.setString(1, rol);
        stmt.setString(2, alias);
        stmt.setString(3, Crypt.Encrypt(clave));
        stmt.setBoolean(4, verificado);
        stmt.setBoolean(5, estado);
        stmt.setInt(6, usercreation.Id);
        stmt.setString(7, correo);
        
        rowupdate = stmt.executeUpdate();
        _conn.close();
        
        if (rowupdate < 0) {
                throw new Exception("No se logro registrar el usuario");
        }
    }
    
    public void UpdateUser(String rol, String alias, String clave, boolean verificado, 
            boolean estado, String correo, int id) throws SQLException, ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;
        
        query =   "UPDATE USUARIOS "
                + "SET rol = ?, alias = ?, clave = ?, verificado = ?, "
                + "fecha_actualizacion = NOW(), estado = ?, correo = ? "
                + "WHERE id = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        
        stmt.setString(1, rol);
        stmt.setString(2, alias);
        stmt.setString(3, Crypt.Encrypt(clave));
        stmt.setBoolean(4, verificado);
        stmt.setBoolean(5, estado);
        stmt.setString(6, correo);
        stmt.setInt(7, id);
        
        rowupdate = stmt.executeUpdate();
        _conn.close();
        
        if (rowupdate < 0) {
                throw new Exception("No se logro actualizar el usuario");
        }
    }
    
    public void DeleteUser(int id) throws SQLException, ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;
        
        query =   "DELETE FROM USUARIOS WHERE id = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, id);
        
        rowupdate = stmt.executeUpdate();
        _conn.close();
        
        if (rowupdate < 0) {
                throw new Exception("No se logro eliminar el usuario");
        }
    }
    
    public void ActiveUnactiveUser(int id) throws SQLException, ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;
        
        query =   "UPDATE USUARIOS SET fecha_actualizacion = NOW(), estado = CASE estado WHEN 0 THEN 1 WHEN 1 THEN 0 END WHERE id = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, id);
        
        rowupdate = stmt.executeUpdate();
        _conn.close();
        
        if (rowupdate < 0) {
                throw new Exception("No se logro cambiar el estado del usuario.");
        }
    }
    
    public void VerificateUser(int id) throws SQLException, ClassNotFoundException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;
        
        query =   "UPDATE USUARIOS SET fecha_actualizacion = NOW(), verificado = 1 WHERE id = ?";
        
        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, id);
        
        rowupdate = stmt.executeUpdate();
        _conn.close();
        
        if (rowupdate < 0) {
                throw new Exception("No se logro cambiar el estado del usuario.");
        }
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Candidate"> 
    
    public Candidate GetCandidate(int userId) throws SQLException, ClassNotFoundException {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        Candidate candidate;

        query = "SELECT * FROM CANDIDATOS WHERE id_usuario = ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, userId);
        rs = stmt.executeQuery();
        rs.next();

        candidate = new Candidate();
        candidate.Id = rs.getInt("id");
        candidate.Cedula = rs.getInt("cedula");
        candidate.Nombre = rs.getString("nombre");
        candidate.Apellido = rs.getString("apellido");
        candidate.Genero = rs.getString("genero").charAt(0);
        candidate.Descripcion = rs.getString("descripcion");
        candidate.FechaNacimiento = rs.getDate("fecha_nacimiento");
        candidate.Estatura = rs.getInt("estatura");
        candidate.Peso = rs.getInt("peso");
        candidate.EstadoCivil = rs.getString("estado_civil").charAt(0);
        candidate.Foto = rs.getBytes("foto");
        candidate.Telefono = rs.getString("telefono");
        candidate.Direccion = rs.getString("direccion");
        candidate.Facebook = rs.getString("facebook");
        candidate.Twitter = rs.getString("twitter");
        candidate.Instagram = rs.getString("instagram");
        candidate.LinkedIn = rs.getString("linked_in");
        candidate.IndVehiculo = rs.getBoolean("ind_vehiculo");
        candidate.IdiomaNativo = rs.getString("idioma_nativo");
        candidate.PaisNacimiento = rs.getString("pais_nacimiento");
        candidate.DepartamentoNacimiento = rs.getString("departamento_nacimiento");
        candidate.CiudadNacimiento = rs.getString("ciudad_nacimiento");
        candidate.PaisResidencia = rs.getString("pais_residencia");
        candidate.DepartamentoResidencia = rs.getString("departamento_residencia");
        candidate.CiudadResidencia = rs.getString("ciudad_residencia");
        candidate.IdUsuario = rs.getInt("id_usuario");

        _conn.close();

        return candidate;
    }
    
    public void CreateCandidate(int userId, int cedula, String nombre, String apellido, char genero, String descripcion, Date fechaNacimiento, int estatura, int peso, char estadoCivil, byte[] foto, String telefono, String direccion, String facebook, String twitter, String instagram, String linkedIn, boolean indVehiculo, String idiomaNativo, String paisNacimiento, String departamentoNacimiento, String ciudadNacimiento, String paisResidencia, String departamentoResidencia, String ciudadResidencia) throws SQLException, ClassNotFoundException, Exception {
    String query;
    PreparedStatement stmt;
    int rowupdate;
    
    query = "INSERT INTO CANDIDATOS (id_usuario, cedula, nombre, apellido, genero, descripcion, fecha_nacimiento, estatura, peso, estado_civil, foto, telefono, direccion, facebook, twitter, instagram, linked_in, ind_vehiculo, idioma_nativo, pais_nacimiento, departamento_nacimiento, ciudad_nacimiento, pais_residencia, departamento_residencia, ciudad_residencia) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    Connect();
    stmt = _conn.prepareStatement(query);
    
    stmt.setInt(1, userId);
    stmt.setInt(2, cedula);
    stmt.setString(3, nombre);
    stmt.setString(4, apellido);
    stmt.setString(5, String.valueOf(genero));
    stmt.setString(6, descripcion);
    stmt.setDate(7, new java.sql.Date(fechaNacimiento.getTime()));
    stmt.setInt(8, estatura);
    stmt.setInt(9, peso);
    stmt.setString(10, String.valueOf(estadoCivil));
    stmt.setBytes(11, foto);
    stmt.setString(12, telefono);
    stmt.setString(13, direccion);
    stmt.setString(14, facebook);
    stmt.setString(15, twitter);
    stmt.setString(16, instagram);
    stmt.setString(17, linkedIn);
    stmt.setBoolean(18, indVehiculo);
    stmt.setString(19, idiomaNativo);
    stmt.setString(20, paisNacimiento);
    stmt.setString(21, departamentoNacimiento);
    stmt.setString(22, ciudadNacimiento);
    stmt.setString(23, paisResidencia);
    stmt.setString(24, departamentoResidencia);
    stmt.setString(25, ciudadResidencia);
    
    rowupdate = stmt.executeUpdate();
    _conn.close();
    
    if (rowupdate < 0) {
        throw new Exception("No se logró registrar el candidato");
    }
}
    
    public void UpdateCandidate(int id, int cedula, String nombre, String apellido, String descripcion, Date fechaNacimiento,
        int estatura, int peso, char estadoCivil, byte[] foto, String telefono, String direccion,
        String facebook, String twitter, String instagram, String linkedIn, boolean indVehiculo, String idiomaNativo,
        String paisNacimiento, String departamentoNacimiento, String ciudadNacimiento, String paisResidencia,
        String departamentoResidencia, String ciudadResidencia, int idUsuario) throws SQLException, ClassNotFoundException, Exception {

        String query;
        PreparedStatement stmt;
        int rowUpdate;

        query = "UPDATE CANDIDATOS "
                + "SET cedula = ?, nombre = ?, apellido = ?, descripcion = ?, fecha_nacimiento = ?, estatura = ?, "
                + "peso = ?, estado_civil = ?, foto = ?, telefono = ?, direccion = ?, facebook = ?, "
                + "twitter = ?, instagram = ?, linked_in = ?, ind_vehiculo = ?, idioma_nativo = ?, pais_nacimiento = ?, "
                + "departamento_nacimiento = ?, ciudad_nacimiento = ?, pais_residencia = ?, departamento_residencia = ?, "
                + "ciudad_residencia = ?, id_usuario = ? "
                + "WHERE id = ?";

        Connect();
        stmt = _conn.prepareStatement(query);

        stmt.setInt(1, cedula);
        stmt.setString(2, nombre);
        stmt.setString(3, apellido);
        stmt.setString(4, descripcion);
        stmt.setDate(5, new java.sql.Date(fechaNacimiento.getTime()));;
        stmt.setInt(6, estatura);
        stmt.setInt(7, peso);
        stmt.setString(8, String.valueOf(estadoCivil));
        stmt.setBytes(9, foto);
        stmt.setString(10, telefono);
        stmt.setString(11, direccion);
        stmt.setString(12, facebook);
        stmt.setString(13, twitter);
        stmt.setString(14, instagram);
        stmt.setString(15, linkedIn);
        stmt.setBoolean(16, indVehiculo);
        stmt.setString(17, idiomaNativo);
        stmt.setString(18, paisNacimiento);
        stmt.setString(19, departamentoNacimiento);
        stmt.setString(20, ciudadNacimiento);
        stmt.setString(21, paisResidencia);
        stmt.setString(22, departamentoResidencia);
        stmt.setString(23, ciudadResidencia);
        stmt.setInt(24, idUsuario);
        stmt.setInt(25, id);

        rowUpdate = stmt.executeUpdate();
        _conn.close();

        if (rowUpdate < 0) {
            throw new Exception("No se logró actualizar el candidato");
        }
    }

    public void DeleteCandidate(int id) throws SQLException, ClassNotFoundException, Exception {
        String query;
        PreparedStatement stmt;
        int rowUpdate;

        query = "DELETE FROM CANDIDATOS WHERE id = ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, id);

        rowUpdate = stmt.executeUpdate();
        _conn.close();

        if (rowUpdate < 0) {
            throw new Exception("No se logró eliminar el candidato");
        }
    }
    
    public List<Candidate> GetCandidates(String filtro) throws SQLException, ClassNotFoundException {
    List<Candidate> candidates;
    String query;
    PreparedStatement stmt;
    ResultSet rs;
    
    candidates = new ArrayList<>();
    
    Connect();
    
    if(IsNullOrEmpty(filtro))
    {
        query = "SELECT * FROM CANDIDATOS";
        stmt = _conn.prepareStatement(query);
    }
    else
    {
        query = "SELECT * FROM CANDIDATOS WHERE nombre LIKE ? OR apellido LIKE ?";
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, "%" + filtro + "%");
        stmt.setString(2, "%" + filtro + "%");
    }
    
    rs = stmt.executeQuery();
    
    while (rs.next()) {
        Candidate candidate = new Candidate();
        candidate.Id = rs.getInt("id");
        candidate.Cedula = rs.getInt("cedula");
        candidate.Nombre = rs.getString("nombre");
        candidate.Apellido = rs.getString("apellido");
        candidate.Descripcion = rs.getString("descripcion");
        candidate.FechaNacimiento = rs.getDate("fecha_nacimiento");
        candidate.Estatura = rs.getInt("estatura");
        candidate.Peso = rs.getInt("peso");
        candidate.EstadoCivil = rs.getString("estado_civil").charAt(0);
        candidate.Foto = rs.getBytes("foto");
        candidate.Telefono = rs.getString("telefono");
        candidate.Direccion = rs.getString("direccion");
        candidate.Facebook = rs.getString("facebook");
        candidate.Twitter = rs.getString("twitter");
        candidate.Instagram = rs.getString("instagram");
        candidate.LinkedIn = rs.getString("linked_in");
        candidate.IndVehiculo = rs.getBoolean("ind_vehiculo");
        candidate.IdiomaNativo = rs.getString("idioma_nativo");
        candidate.PaisNacimiento = rs.getString("pais_nacimiento");
        candidate.DepartamentoNacimiento = rs.getString("departamento_nacimiento");
        candidate.CiudadNacimiento = rs.getString("ciudad_nacimiento");
        candidate.PaisResidencia = rs.getString("pais_residencia");
        candidate.DepartamentoResidencia = rs.getString("departamento_residencia");
        candidate.CiudadResidencia = rs.getString("ciudad_residencia");
        candidate.IdUsuario = rs.getInt("id_usuario");
        
        candidates.add(candidate);
    }
    
    _conn.close();
    
    return candidates;
}
    // </editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Habilities"> 
    
    public DefaultTableModel GetHabilities(int userid) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        DefaultTableModel model;
        
        model = new DefaultTableModel();
        
        model.addColumn("Nombre");
        model.addColumn("Nivel");
        model.addColumn("Comentario");
        
        query = "SELECT COM.nombre, HAB.nivel, HAB.comentario "
            + "FROM HABILIDADES HAB "
            + "INNER JOIN COMPETENCIAS COM ON COM.id = HAB.id_competencia "
            + "INNER JOIN CANDIDATOS CAN ON CAN.id = HAB.id_candidato "
            + "INNER JOIN USUARIOS USU ON USU.id = CAN.id_usuario "
            + "WHERE USU.id = ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, userid);
        rs = stmt.executeQuery();
        
        while (rs.next()) 
        {
            Object[] row = {rs.getString("Nombre"), rs.getString("Nivel"), rs.getString("Comentario")};
            model.addRow(row);
        }
        
        _conn.close();
        
        return model;
    }
    
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Skills"> 
    
    // Método GetCompetences para obtener una lista de Competence filtrando por nombre o categoría

    public List<Competence> GetCompetences(String filtro) throws SQLException, ClassNotFoundException {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        List<Competence> competences = new ArrayList<>();

        query = "SELECT id, nombre, categoria, certificada, criterio_medicion FROM COMPETENCIAS WHERE nombre LIKE ? OR categoria LIKE ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, "%" + filtro + "%");
        stmt.setString(2, "%" + filtro + "%");
        rs = stmt.executeQuery();

        while (rs.next()) {
            competences.add(new Competence(
                    rs.getInt("id"),
                    rs.getString("nombre"),
                    rs.getString("categoria"),
                    rs.getBoolean("certificada"),
                    rs.getString("criterio_medicion")
            ));
        }
        _conn.close();

        return competences;
    }
    
    public DefaultTableModel GetUsersByCompetence(int competenceid) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        DefaultTableModel model;
        
        model = new DefaultTableModel();
        
        model.addColumn("Nivel");
        model.addColumn("Apellido");
        model.addColumn("Nombre");        
        model.addColumn("Correo");

        
        query = "SELECT HAB.nivel 'Nivel', "
            + "CAN.apellido 'Apellido', "
            + "CAN.nombre 'Nombre', "
            + "USU.correo 'Correo' "
            + "FROM CANDIDATOS CAN "
            + "INNER JOIN HABILIDADES HAB ON HAB.id_candidato = CAN.id "
            + "INNER JOIN COMPETENCIAS COM ON COM.id = HAB.id_competencia "
            + "INNER JOIN USUARIOS USU ON USU.id = CAN.id_usuario "
            + "WHERE COM.id = ? "
            + "ORDER BY HAB.nivel DESC";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, competenceid);
        rs = stmt.executeQuery();
        
        while (rs.next()) 
        {
            Object[] row = {rs.getString("Nivel"), rs.getString("Apellido"), rs.getString("Nombre"), rs.getString("Correo")};
            model.addRow(row);
        }
        
        _conn.close();
        
        return model;
    }
    
    public DefaultTableModel GetOffersByCompetence(int competenceid) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        DefaultTableModel model;
        
        model = new DefaultTableModel();
        
        model.addColumn("Numero");
        model.addColumn("Empresa");
        model.addColumn("Oferta");        
        model.addColumn("Salario base");
        model.addColumn("Salario máximo");

        query = "SELECT OFE.id 'Numero', "
            + "EMP.razon_social 'Empresa', "
            + "OFE.descripcion 'Oferta', "
            + "OFE.salario_base 'Salario base', "
            + "OFE.salario_maximo 'Salario máximo' "
            + "FROM OFERTAS OFE "
            + "INNER JOIN EMPRESAS EMP ON EMP.id = OFE.id_empresa "
            + "INNER JOIN REQUERIMIENTOS REQ ON REQ.id_oferta = OFE.id "
            + "INNER JOIN COMPETENCIAS COMP ON COMP.id = REQ.id_competencia "
            + "WHERE COMP.id = ? "
            + "ORDER BY OFE.salario_base DESC";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, competenceid);
        rs = stmt.executeQuery();
        
        while (rs.next()) 
        {
            Object[] row = {rs.getString("Numero"), rs.getString("Empresa"), rs.getString("Oferta"), rs.getString("Salario base"), rs.getString("Salario máximo")};
            model.addRow(row);
        }
        
        _conn.close();
        
        return model;
    }
    
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Offerts"> 
    
    public List<Offer> GetOffers(String filter) throws SQLException, ClassNotFoundException 
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        List<Offer> offers = new ArrayList<>();

        query = "SELECT O.id 'Id', "
            + "E.razon_social 'Empresa', "
            + "O.descripcion 'Descripción', "
            + "O.salario_base 'Salario base', "
            + "O.vacantes 'Vacantes', "
            + "O.dias_semanales 'Días semanales', "
            + "O.expiracion 'Vencimiento', "
            + "COUNT(1) 'Requerimientos' "
            + "FROM OFERTAS O "
            + "INNER JOIN EMPRESAS E ON E.id = O.id_empresa "
            + "INNER JOIN REQUERIMIENTOS R ON R.id_oferta = O.id "
            + "WHERE O.Nombre LIKE ? OR E.razon_social LIKE ? OR O.descripcion LIKE ? OR O.cargo LIKE ? "
            + "GROUP BY O.id";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, "%" + filter + "%");
        stmt.setString(2, "%" + filter + "%");
        stmt.setString(3, "%" + filter + "%");
        stmt.setString(4, "%" + filter + "%");
        rs = stmt.executeQuery();

        while (rs.next()) {
            Offer offer = new Offer();
            offer.Id = rs.getInt("Id");
            offer.NombreEmpresa = rs.getString("Empresa");
            offer.Descripcion = rs.getString("Descripción");
            offer.SalarioBase = rs.getBigDecimal("Salario base");
            offer.Vacantes = rs.getInt("Vacantes");
            offer.DiasSemanales = rs.getInt("Días semanales");
            offer.Expiracion = rs.getDate("Vencimiento");
            offer.NumRequerimientos = rs.getInt("Requerimientos");

            offers.add(offer);
        }

        _conn.close();

        return offers;
    }
    
    public DefaultTableModel GetCompetencesByOffer(int offerid) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        DefaultTableModel model;
        
        model = new DefaultTableModel();
        
        model.addColumn("Id");
        model.addColumn("Nombre");
        model.addColumn("Categoria");        
        model.addColumn("Certificación");
        model.addColumn("Criterio");

        
        query = "SELECT COM.id 'Id', "
            + "COM.nombre 'Nombre', "
            + "COM.categoria 'Categoria', "
            + "CASE "
            + "    WHEN COM.certificada = 0 THEN 'No' "
            + "    WHEN COM.certificada = 1 THEN 'Si' "
            + "END 'Certificación', "
            + "COM.criterio_medicion 'Criterio' "
            + "FROM COMPETENCIAS COM "
            + "INNER JOIN REQUERIMIENTOS REQ ON REQ.id_competencia = COM.id "
            + "INNER JOIN OFERTAS OFE ON OFE.id = REQ.id_oferta "
            + "WHERE REQ.id_oferta = ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, offerid);
        rs = stmt.executeQuery();
        
        while (rs.next()) 
        {
            Object[] row = {rs.getString("Id"), rs.getString("Nombre"), rs.getString("Categoria"), rs.getString("Certificación"), rs.getString("Criterio")};
            model.addRow(row);
        }
        
        _conn.close();
        
        return model;
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Postulations">
    
    public void CreatePostulation(int id_candidato, int id_oferta, String comentario_candidato) throws SQLException, Exception
    {
        String query;
        PreparedStatement stmt;
        int rowupdate;

        query = "INSERT INTO POSTULACIONES (id_candidato, id_oferta, fecha_registro, comentario_candidato) "
                + "VALUES (?, ?, NOW(), ?)";

        Connect();
        stmt = _conn.prepareStatement(query);

        stmt.setInt(1, id_candidato);
        stmt.setInt(2, id_oferta);
        stmt.setString(3, comentario_candidato);
        
        rowupdate = stmt.executeUpdate();
        _conn.close();

        if (rowupdate < 0) {
            throw new Exception("No se logró registrar el candidato");
        }
    }
    
    public DefaultTableModel GetPostulationsByCandidate(int id_candidate, String filter) throws SQLException, 
            ClassNotFoundException
    {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        DefaultTableModel model;
        
        model = new DefaultTableModel();
        
        model.addColumn("OFERTA");
        model.addColumn("Empresa");
        model.addColumn("Nombre");
        model.addColumn("Salario base");        
        model.addColumn("Salario máximo");
        model.addColumn("Fecha registro");
        model.addColumn("Comentario empresa");
        
        query = "SELECT OFE.id 'OFERTA', "
            + "EMP.razon_social 'Empresa', "
            + "OFE.nombre 'Nombre', "
            + "OFE.salario_base 'Salario base', "
            + "OFE.salario_maximo 'Salario máximo', "
            + "POS.fecha_registro 'Fecha registro', "
            + "POS.comentario_empresa 'Comentario empresa' "
            + "FROM POSTULACIONES POS "
            + "INNER JOIN OFERTAS OFE ON OFE.id = POS.id_oferta "
            + "INNER JOIN EMPRESAS EMP ON EMP.id = OFE.id_empresa "
            + "INNER JOIN CANDIDATOS CAN ON CAN.id = POS.id_candidato "
            + "INNER JOIN USUARIOS USU ON USU.id = CAN.id_usuario "
            + "WHERE CAN.id = ? "
            + "ORDER BY POS.fecha_registro DESC";
        
        if(IsNullOrEmpty(filter))
        {
            query = "SELECT OFE.id 'OFERTA', "
            + "EMP.razon_social 'Empresa', "
            + "OFE.nombre 'Nombre', "
            + "OFE.salario_base 'Salario base', "
            + "OFE.salario_maximo 'Salario máximo', "
            + "POS.fecha_registro 'Fecha registro', "
            + "POS.comentario_empresa 'Comentario empresa' "
            + "FROM POSTULACIONES POS "
            + "INNER JOIN OFERTAS OFE ON OFE.id = POS.id_oferta "
            + "INNER JOIN EMPRESAS EMP ON EMP.id = OFE.id_empresa "
            + "INNER JOIN CANDIDATOS CAN ON CAN.id = POS.id_candidato "
            + "INNER JOIN USUARIOS USU ON USU.id = CAN.id_usuario "
            + "WHERE CAN.id = ? "
            + "ORDER BY POS.fecha_registro DESC";
            
            Connect();
            stmt = _conn.prepareStatement(query);
            stmt.setInt(1, id_candidate);
        }
        else
        {
            query = "SELECT OFE.id 'OFERTA', "
            + "EMP.razon_social 'Empresa', "
            + "OFE.nombre 'Nombre', "
            + "OFE.salario_base 'Salario base', "
            + "OFE.salario_maximo 'Salario máximo', "
            + "POS.fecha_registro 'Fecha registro', "
            + "POS.comentario_empresa 'Comentario empresa' "
            + "FROM POSTULACIONES POS "
            + "INNER JOIN OFERTAS OFE ON OFE.id = POS.id_oferta "
            + "INNER JOIN EMPRESAS EMP ON EMP.id = OFE.id_empresa "
            + "INNER JOIN CANDIDATOS CAN ON CAN.id = POS.id_candidato "
            + "INNER JOIN USUARIOS USU ON USU.id = CAN.id_usuario "
            + "WHERE CAN.id = ? "
            + "AND (EMP.razon_social LIKE ? OR OFE.nombre LIKE ?) "
            + "ORDER BY POS.fecha_registro DESC";
            
            Connect();
            stmt = _conn.prepareStatement(query);
            stmt.setInt(1, id_candidate);
            stmt.setString(2, "%" + filter + "%");
            stmt.setString(3, "%" + filter + "%");
        }

        rs = stmt.executeQuery();
        
        while (rs.next()) 
        {
            Object[] row = {rs.getString("OFERTA"), rs.getString("Empresa"), rs.getString("Nombre"), rs.getString("Salario base"), rs.getString("Salario máximo"), rs.getString("Fecha registro"), rs.getString("Comentario empresa")};
            model.addRow(row);
        }
        
        _conn.close();
        
        return model;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Company">
         public List<Company> getCompanies(String alias) throws SQLException, ClassNotFoundException {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        List<Company> companies;

        companies = new ArrayList<>();

        if (IsNullOrEmpty(alias)) {
            query = "SELECT id, nit, razon_social, fecha_fundacion, alias, regimen, categoria, numero_contacto, "
                    + "numero_contacto_aux, correo_contacto, nombre_contacto, cargo_contacto, sitio_web, mision, "
                    + "vision, valores, prom_salarial, pais, ciudad, tipo_empresa, num_empleados, direccion, "
                    + "facebook, twitter, instagram, linkedin, id_usuario FROM COMPANIES";
        } else {
            query = "SELECT id, nit, razon_social, fecha_fundacion, alias, regimen, categoria, numero_contacto, "
                    + "numero_contacto_aux, correo_contacto, nombre_contacto, cargo_contacto, sitio_web, mision, "
                    + "vision, valores, prom_salarial, pais, ciudad, tipo_empresa, num_empleados, direccion, "
                    + "facebook, twitter, instagram, linkedin, id_usuario FROM COMPANIES WHERE alias LIKE ?";
        }

        Connect();
        stmt = _conn.prepareStatement(query);
        if (!IsNullOrEmpty(alias)) stmt.setString(1, "%" + alias + "%");
        rs = stmt.executeQuery();


            while (rs.next()) {
                Company company = new Company();
                company.Id = rs.getInt("id");
                company.Nit = rs.getInt("nit");
                company.RazonSocial = rs.getString("razon_social");
                company.FechaFundacion = rs.getDate("fecha_fundacion");
                company.Alias = rs.getString("alias");
                company.Regimen = rs.getString("regimen").charAt(0);
                company.Categoria = rs.getString("categoria").charAt(0);
                company.NumeroContacto = rs.getInt("numero_contacto");
                company.NumeroContactoAux = rs.getInt("numero_contacto_aux");
                company.CorreoContacto = rs.getString("correo_contacto");
                company.NombreContacto = rs.getString("nombre_contacto");
                company.CargoContacto = rs.getString("cargo_contacto");
                company.SitioWeb = rs.getString("sitio_web");
                company.Mision = rs.getString("mision");
                company.Vision = rs.getString("vision");
                company.Valores = rs.getString("valores");
                company.PromSalarial = rs.getInt("prom_salarial");
                company.Pais = rs.getString("pais");
                company.Ciudad = rs.getString("ciudad");
                company.TipoEmpresa = rs.getString("tipo_empresa");
                company.NumEmpleados = rs.getInt("num_empleados");
                company.Direccion = rs.getString("direccion");
                company.Facebook = rs.getString("facebook");
                company.Twitter = rs.getString("twitter");
                company.Instagram = rs.getString("instagram");
                company.LinkedIn = rs.getString("linkedin");
                company.IdUsuario = rs.getInt("id_usuario");
                company.Departamento = rs.getString("departamento");

                companies.add(company);
    }
    _conn.close();

    return companies;
    }    
        public Company getCompany(String alias) throws SQLException, ClassNotFoundException {
        String query;
        PreparedStatement stmt;
        ResultSet rs;
        Company company;

        query = "SELECT id, nit, razon_social, fecha_fundacion, alias, regimen, categoria, numero_contacto, "
                + "numero_contacto_aux, correo_contacto, nombre_contacto, cargo_contacto, sitio_web, mision, "
                + "vision, valores, prom_salarial, pais, ciudad, tipo_empresa, num_empleados, direccion, "
                + "facebook, twitter, instagram, linkedin, id_usuario FROM COMPANIES WHERE alias = ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setString(1, alias);
        rs = stmt.executeQuery();
        rs.next();

        company = new Company();
        company.Id = rs.getInt("id");
        company.Nit = rs.getInt("nit");
        company.RazonSocial = rs.getString("razon_social");
        company.FechaFundacion = rs.getDate("fecha_fundacion");
        company.Alias = rs.getString("alias");
        company.Regimen = rs.getString("regimen").charAt(0);
        company.Categoria = rs.getString("categoria").charAt(0);
        company.NumeroContacto = rs.getInt("numero_contacto");
        company.NumeroContactoAux = rs.getInt("numero_contacto_aux");
        company.CorreoContacto = rs.getString("correo_contacto");
        company.NombreContacto = rs.getString("nombre_contacto");
        company.CargoContacto = rs.getString("cargo_contacto");
        company.SitioWeb = rs.getString("sitio_web");
        company.Mision = rs.getString("mision");
        company.Vision = rs.getString("vision");
        company.Valores = rs.getString("valores");
        company.PromSalarial = rs.getInt("prom_salarial");
        company.Pais = rs.getString("pais");
        company.Ciudad = rs.getString("ciudad");
        company.TipoEmpresa = rs.getString("tipo_empresa");
        company.NumEmpleados = rs.getInt("num_empleados");
        company.Direccion = rs.getString("direccion");
        company.Facebook = rs.getString("facebook");
        company.Twitter = rs.getString("twitter");
        company.Instagram = rs.getString("instagram");
        company.LinkedIn = rs.getString("linkedin");
        company.IdUsuario = rs.getInt("id_usuario");
        company.Departamento = rs.getString("departamento");

        _conn.close();

        return company;
    }
        public void CreateCompany(Company company) throws SQLException, ClassNotFoundException, Exception {
        String query;
        PreparedStatement stmt;
        int rowUpdate;

        query = "INSERT INTO COMPANIES (id, nit, razon_social, fecha_fundacion, alias, regimen, categoria, numero_contacto, "
                + "numero_contacto_aux, correo_contacto, nombre_contacto, cargo_contacto, sitio_web, mision, vision, valores, "
                + "prom_salarial, pais, ciudad, tipo_empresa, num_empleados, direccion, facebook, twitter, instagram, linkedin, "
                + "id_usuario, departamento) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        Connect();
        stmt = _conn.prepareStatement(query);

        stmt.setInt(1, company.Id);
        stmt.setInt(2, company.Nit);
        stmt.setString(3, company.RazonSocial);
        stmt.setDate(4, new java.sql.Date(company.FechaFundacion.getTime()));
        stmt.setString(5, company.Alias);
        stmt.setString(6, String.valueOf(company.Regimen));
        stmt.setString(7, String.valueOf(company.Categoria));
        stmt.setInt(8, company.NumeroContacto);
        stmt.setInt(9, company.NumeroContactoAux);
        stmt.setString(10, company.CorreoContacto);
        stmt.setString(11, company.NombreContacto);
        stmt.setString(12, company.CargoContacto);
        stmt.setString(13, company.SitioWeb);
        stmt.setString(14, company.Mision);
        stmt.setString(15, company.Vision);
        stmt.setString(16, company.Valores);
        stmt.setInt(17, company.PromSalarial);
        stmt.setString(18, company.Pais);
        stmt.setString(19, company.Ciudad);
        stmt.setString(20, company.TipoEmpresa);
        stmt.setInt(21, company.NumEmpleados);
        stmt.setString(22, company.Direccion);
        stmt.setString(23, company.Facebook);
        stmt.setString(24, company.Twitter);
        stmt.setString(25, company.Instagram);
        stmt.setString(26, company.LinkedIn);
        stmt.setInt(27, company.IdUsuario);
        stmt.setString(28, company.Departamento);

        rowUpdate = stmt.executeUpdate();
        _conn.close();

        if (rowUpdate < 0) {
            throw new Exception("No se logró registrar la compañía");
        }
    }
        
    public void UpdateCompany(Company company) throws SQLException, ClassNotFoundException, Exception {
        String query;
        PreparedStatement stmt;
        int rowUpdate;

        query = "UPDATE COMPANIES "
                + "SET nit = ?, razon_social = ?, fecha_fundacion = ?, alias = ?, regimen = ?, categoria = ?, "
                + "numero_contacto = ?, numero_contacto_aux = ?, correo_contacto = ?, nombre_contacto = ?, "
                + "cargo_contacto = ?, sitio_web = ?, mision = ?, vision = ?, valores = ?, prom_salarial = ?, "
                + "pais = ?, ciudad = ?, tipo_empresa = ?, num_empleados = ?, direccion = ?, facebook = ?, "
                + "twitter = ?, instagram = ?, linkedin = ?, id_usuario = ? "
                + "WHERE id = ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, company.Id);
        stmt.setInt(2, company.Nit);
        stmt.setString(3, company.RazonSocial);
        stmt.setDate(4, new java.sql.Date(company.FechaFundacion.getTime()));
        stmt.setString(5, company.Alias);
        stmt.setString(6, String.valueOf(company.Regimen));
        stmt.setString(7, String.valueOf(company.Categoria));
        stmt.setInt(8, company.NumeroContacto);
        stmt.setInt(9, company.NumeroContactoAux);
        stmt.setString(10, company.CorreoContacto);
        stmt.setString(11, company.NombreContacto);
        stmt.setString(12, company.CargoContacto);
        stmt.setString(13, company.SitioWeb);
        stmt.setString(14, company.Mision);
        stmt.setString(15, company.Vision);
        stmt.setString(16, company.Valores);
        stmt.setInt(17, company.PromSalarial);
        stmt.setString(18, company.Pais);
        stmt.setString(19, company.Ciudad);
        stmt.setString(20, company.TipoEmpresa);
        stmt.setInt(21, company.NumEmpleados);
        stmt.setString(22, company.Direccion);
        stmt.setString(23, company.Facebook);
        stmt.setString(24, company.Twitter);
        stmt.setString(25, company.Instagram);
        stmt.setString(26, company.LinkedIn);
        stmt.setInt(27, company.IdUsuario);
        stmt.setInt(28, company.Id);

        rowUpdate = stmt.executeUpdate();
        _conn.close();

        if (rowUpdate < 0) {
            throw new Exception("No se logró actualizar la compañía");
        }
    }

    public void DeleteCompany(int id) throws SQLException, ClassNotFoundException, Exception {
        String query;
        PreparedStatement stmt;
        int rowUpdate;

        query = "DELETE FROM COMPANIES WHERE id = ?";

        Connect();
        stmt = _conn.prepareStatement(query);
        stmt.setInt(1, id);

        rowUpdate = stmt.executeUpdate();
        _conn.close();

        if (rowUpdate < 0) {
            throw new Exception("No se logró eliminar la compañía");
        }
    }
        // </editor-fold>
}
