/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Forms;

import Connection.GEEConnection;
import Entities.*;
import Panels.panCandidate;
import Services.Notifications;
import static Services.Notifications.*;
import java.sql.SQLException;
import javax.swing.JPanel;
import Panels.panStudy;


/**
 *
 * @author diegosantacruz
 */
public class frmMainPanel extends javax.swing.JFrame {

    private Connection.GEEConnection _connection;
    private User _user;
    private String _alias;
    
    public frmMainPanel(String alias) throws SQLException, ClassNotFoundException {
        initComponents();
        
        _alias = alias;
        _connection = new GEEConnection();
    }

    private frmMainPanel() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem2 = new javax.swing.JMenuItem();
        panMain = new javax.swing.JPanel();
        tpnMain = new javax.swing.JTabbedPane();
        panFooter = new javax.swing.JPanel();
        lblConexion = new javax.swing.JLabel();
        mnbMain = new javax.swing.JMenuBar();
        menArchivo = new javax.swing.JMenu();
        mniCamClave = new javax.swing.JMenuItem();
        mniCerrarSesion = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mniSalir = new javax.swing.JMenuItem();
        menAdmin = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        menNueComp = new javax.swing.JMenuItem();
        menListComp = new javax.swing.JMenuItem();
        mnmUsuarios = new javax.swing.JMenu();
        mniUsuariosCrear = new javax.swing.JMenuItem();
        mniUsuariosList = new javax.swing.JMenuItem();
        menCandidato = new javax.swing.JMenu();
        menAgreEstudios = new javax.swing.JMenuItem();
        menAgreExperi = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menEstudios = new javax.swing.JMenuItem();
        menExperiencias = new javax.swing.JMenuItem();
        mniCandiInformacion = new javax.swing.JMenuItem();
        menMisPostu = new javax.swing.JMenuItem();
        menEmpresa = new javax.swing.JMenu();
        mniEmpInformacion = new javax.swing.JMenuItem();
        mniEmpCamClave = new javax.swing.JMenuItem();
        menConsultasC = new javax.swing.JMenu();
        menOffers = new javax.swing.JMenuItem();
        menCompetencesSkills = new javax.swing.JMenuItem();
        menConsultasE = new javax.swing.JMenu();
        menCandidatos = new javax.swing.JMenuItem();
        menCompetencias = new javax.swing.JMenuItem();
        menOfertas = new javax.swing.JMenuItem();

        jMenuItem2.setText("jMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("GEE");
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setMinimumSize(new java.awt.Dimension(694, 670));
        setPreferredSize(new java.awt.Dimension(694, 670));
        setSize(new java.awt.Dimension(694, 670));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });

        panMain.setLayout(new java.awt.BorderLayout());
        panMain.add(tpnMain, java.awt.BorderLayout.CENTER);

        panFooter.setBackground(new java.awt.Color(255, 255, 255));
        panFooter.setAlignmentX(0.0F);
        panFooter.setAlignmentY(0.0F);
        panFooter.setLayout(new java.awt.BorderLayout());

        lblConexion.setText("lblConexion");
        panFooter.add(lblConexion, java.awt.BorderLayout.LINE_START);

        panMain.add(panFooter, java.awt.BorderLayout.SOUTH);

        getContentPane().add(panMain, java.awt.BorderLayout.CENTER);

        mnbMain.setAutoscrolls(true);

        menArchivo.setText("Archivo");

        mniCamClave.setText("Cambiar contraseña...");
        mniCamClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniCamClaveActionPerformed(evt);
            }
        });
        menArchivo.add(mniCamClave);

        mniCerrarSesion.setText("Cerrar sesión...");
        mniCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniCerrarSesionActionPerformed(evt);
            }
        });
        menArchivo.add(mniCerrarSesion);
        menArchivo.add(jSeparator1);

        mniSalir.setText("Salir...");
        mniSalir.setToolTipText("Salir del aplicativo");
        mniSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniSalirActionPerformed(evt);
            }
        });
        menArchivo.add(mniSalir);

        mnbMain.add(menArchivo);

        menAdmin.setText("Administración");

        jMenu1.setText("Competencias");

        menNueComp.setText("Nueva...");
        menNueComp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menNueCompActionPerformed(evt);
            }
        });
        jMenu1.add(menNueComp);

        menListComp.setText("Listar...");
        jMenu1.add(menListComp);

        menAdmin.add(jMenu1);

        mnmUsuarios.setText("Usuarios");

        mniUsuariosCrear.setText("Nuevo...");
        mniUsuariosCrear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniUsuariosCrearActionPerformed(evt);
            }
        });
        mnmUsuarios.add(mniUsuariosCrear);

        mniUsuariosList.setLabel("Listar...");
        mniUsuariosList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniUsuariosListActionPerformed(evt);
            }
        });
        mnmUsuarios.add(mniUsuariosList);

        menAdmin.add(mnmUsuarios);

        mnbMain.add(menAdmin);

        menCandidato.setText("Candidato");

        menAgreEstudios.setText("Agregar estudios...");
        menAgreEstudios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menAgreEstudiosActionPerformed(evt);
            }
        });
        menCandidato.add(menAgreEstudios);

        menAgreExperi.setText("Agregar experiencia...");
        menAgreExperi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menAgreExperiActionPerformed(evt);
            }
        });
        menCandidato.add(menAgreExperi);
        menCandidato.add(jSeparator2);

        menEstudios.setText("Estudios...");
        menCandidato.add(menEstudios);

        menExperiencias.setText("Experiencias...");
        menCandidato.add(menExperiencias);

        mniCandiInformacion.setText("Información...");
        menCandidato.add(mniCandiInformacion);

        menMisPostu.setText("Mis postulaciones...");
        menMisPostu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menMisPostuActionPerformed(evt);
            }
        });
        menCandidato.add(menMisPostu);

        mnbMain.add(menCandidato);

        menEmpresa.setText("Empresa");

        mniEmpInformacion.setText("Información...");
        menEmpresa.add(mniEmpInformacion);

        mniEmpCamClave.setText("Cambiar contraseña...");
        mniEmpCamClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mniEmpCamClaveActionPerformed(evt);
            }
        });
        menEmpresa.add(mniEmpCamClave);

        mnbMain.add(menEmpresa);

        menConsultasC.setText("Consultas");

        menOffers.setText("Ofertas...");
        menOffers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menOffersActionPerformed(evt);
            }
        });
        menConsultasC.add(menOffers);

        menCompetencesSkills.setText("Competencias...");
        menCompetencesSkills.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menCompetencesSkillsActionPerformed(evt);
            }
        });
        menConsultasC.add(menCompetencesSkills);

        mnbMain.add(menConsultasC);

        menConsultasE.setText("Consultas");

        menCandidatos.setText("Candidatos...");
        menCandidatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menCandidatosActionPerformed(evt);
            }
        });
        menConsultasE.add(menCandidatos);

        menCompetencias.setText("Competencias...");
        menCompetencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menCompetenciasActionPerformed(evt);
            }
        });
        menConsultasE.add(menCompetencias);

        menOfertas.setText("Ofertas...");
        menOfertas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menOfertasActionPerformed(evt);
            }
        });
        menConsultasE.add(menOfertas);

        mnbMain.add(menConsultasE);

        setJMenuBar(mnbMain);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mniSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniSalirActionPerformed
        System.exit(1);
    }//GEN-LAST:event_mniSalirActionPerformed

    private void mniCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniCerrarSesionActionPerformed
        frmLogin login;
        
        try
        {
            login = new frmLogin();
            login.setVisible(true);
            dispose();
        }
        catch (Exception ex)
        {
            
        }
    }//GEN-LAST:event_mniCerrarSesionActionPerformed

    private void mniUsuariosCrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniUsuariosCrearActionPerformed
        JPanel pan;
        
        pan = new Panels.panUser(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_mniUsuariosCrearActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        try {
            menAdmin.setVisible(false);
            menCandidato.setVisible(false);
            menEmpresa.setVisible(false);
            menConsultasC.setVisible(false);
            menConsultasE.setVisible(false);
            
            lblConexion.setText(_connection.GetConnectDescription());
            _user = _connection.GetUser(_alias);
            
            switch (_user.Rol) {
                case Administrator :
                    menAdmin.setVisible(true);
                    menConsultasC.setVisible(true);
                    menConsultasE.setVisible(true);
                    menConsultasE.setText("Consultas Empresa");
                    menConsultasC.setText("Consultas Candidato");
                    menCandidato.setVisible(true);
                break;
                case Candidate :
                    if(_user.Verificado)
                    {
                        menCandidato.setVisible(true);
                        menConsultasC.setVisible(true);
                    }
                    else
                    {
                        mnbMain.setEnabled(false);
                        Notifications.ShowWarning(this, "Actualmente no esta verificado, debe terminar su registro para continuar.");
                        tpnMain.add(new panCandidate(_connection, _user));
                    }
                break;
                case Company :
                    if(_user.Verificado)
                    {
                        menEmpresa.setVisible(true);
                        menConsultasE.setVisible(true);
                    }
                    else
                    {
                        mnbMain.setEnabled(false);
                        Notifications.ShowWarning(this, "Actualmente no esta verificado, debe terminar su registro para continuar.");
                        //tpnMain.add(new panCandidate());
                    }
                break;
                default : throw new AssertionError();
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ShowError(this, ex.getMessage());
        }
    }//GEN-LAST:event_formComponentShown

    private void mniUsuariosListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniUsuariosListActionPerformed
        JPanel pan;
        
        pan =new Panels.panListUsers(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_mniUsuariosListActionPerformed

    private void mniCamClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniCamClaveActionPerformed
        JPanel pan;
        
        pan = new Panels.panListUsers(_connection, _user);
        tpnMain.add(new Panels.panChgPassword(_connection, _user));
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_mniCamClaveActionPerformed

    private void menCandidatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menCandidatosActionPerformed
        JPanel pan;
        
        pan =new Panels.panListCandidatesHabilities(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_menCandidatosActionPerformed

    private void menCompetenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menCompetenciasActionPerformed
        JPanel pan;
        
        pan =new Panels.panListSkillsCandidates(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_menCompetenciasActionPerformed

    private void menCompetencesSkillsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menCompetencesSkillsActionPerformed
        JPanel pan;
        
        pan =new Panels.panListSkillsOffers(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_menCompetencesSkillsActionPerformed

    private void menOffersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menOffersActionPerformed
        JPanel pan;
        
        pan =new Panels.panListOfferts(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_menOffersActionPerformed

    private void menOfertasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menOfertasActionPerformed
        JPanel pan;
        
        pan =new Panels.panListOfferts(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_menOfertasActionPerformed

    private void menNueCompActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menNueCompActionPerformed
        
    }//GEN-LAST:event_menNueCompActionPerformed

    private void menMisPostuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menMisPostuActionPerformed
        JPanel pan;
        
        pan =new Panels.panMyPostulations(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_menMisPostuActionPerformed

    private void mniEmpCamClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mniEmpCamClaveActionPerformed
        JPanel pan;
        
        pan =new Panels.panChgPassword(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);
    }//GEN-LAST:event_mniEmpCamClaveActionPerformed

    private void menAgreEstudiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menAgreEstudiosActionPerformed
        JPanel pan;
        
        pan =new Panels.panStudy(_connection, _user);
        tpnMain.add(pan);
        tpnMain.setSelectedComponent(pan);        // TODO add your handling code here:
    }//GEN-LAST:event_menAgreEstudiosActionPerformed

    private void menAgreExperiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menAgreExperiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menAgreExperiActionPerformed


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmMainPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmMainPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmMainPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmMainPanel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new frmMainPanel().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JLabel lblConexion;
    private javax.swing.JMenu menAdmin;
    private javax.swing.JMenuItem menAgreEstudios;
    private javax.swing.JMenuItem menAgreExperi;
    private javax.swing.JMenu menArchivo;
    private javax.swing.JMenu menCandidato;
    private javax.swing.JMenuItem menCandidatos;
    private javax.swing.JMenuItem menCompetencesSkills;
    private javax.swing.JMenuItem menCompetencias;
    private javax.swing.JMenu menConsultasC;
    private javax.swing.JMenu menConsultasE;
    private javax.swing.JMenu menEmpresa;
    private javax.swing.JMenuItem menEstudios;
    private javax.swing.JMenuItem menExperiencias;
    private javax.swing.JMenuItem menListComp;
    private javax.swing.JMenuItem menMisPostu;
    private javax.swing.JMenuItem menNueComp;
    private javax.swing.JMenuItem menOfertas;
    private javax.swing.JMenuItem menOffers;
    private javax.swing.JMenuBar mnbMain;
    private javax.swing.JMenuItem mniCamClave;
    private javax.swing.JMenuItem mniCandiInformacion;
    private javax.swing.JMenuItem mniCerrarSesion;
    private javax.swing.JMenuItem mniEmpCamClave;
    private javax.swing.JMenuItem mniEmpInformacion;
    private javax.swing.JMenuItem mniSalir;
    private javax.swing.JMenuItem mniUsuariosCrear;
    private javax.swing.JMenuItem mniUsuariosList;
    private javax.swing.JMenu mnmUsuarios;
    private javax.swing.JPanel panFooter;
    private javax.swing.JPanel panMain;
    private javax.swing.JTabbedPane tpnMain;
    // End of variables declaration//GEN-END:variables
}
